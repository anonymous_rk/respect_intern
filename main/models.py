from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class CustomUser(AbstractUser):
    avatar = models.ImageField(upload_to='user/', max_length=100)


class Offer(models.Model):
    OFFER_TYPE = [
        ('buy', 'buy'),
        ('sell', 'sell')
    ]

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    full_text = models.TextField(max_length=2000)
    price = models.IntegerField()
    picture = models.ImageField(upload_to='offers/', max_length=100)
    offer_type = models.CharField(choices=OFFER_TYPE, max_length=50)

    def __str__(self):
        return self.title


class Comment(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    comment_text = models.TextField(max_length=1000)

    def __str__(self):
        return self.user_id.username + 'comment'


class Categories(models.Model):
    name = models.CharField(max_length=50)
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='categories/')
    slug = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class CategoryOffer(models.Model):
    category = models.ForeignKey(Categories, on_delete=models.CASCADE)
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.category_id) + "-" + str(self.offer_id)
